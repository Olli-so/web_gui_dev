#!/bin/bash

git init 
echo "node_modules" > .gitignore

npm init -y
npm install express --save
npm install mocha chai request --save-dev

mkdir src
mkdir test

touch src/converter.js
touch src/main.js

touch test/converter.spec.js
touch test/main.spec.js

touch README.md

#REMEMBER ADD SCRIPTS TO PACKAGE JSON
# script: "test", "start"
