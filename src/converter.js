/**
 * 
 * @param {string} red 
 * @param {string} green 
 * @param {string} blue 
 */
const rgbToHex = (red, green, blue) => {
    let redHex = parseInt(red).toString(16);
    let greenHex = parseInt(green).toString(16);
    let blueHex = parseInt(blue).toString(16);
    // Problem with the .toString() is that is removes a wanted "zero" from rgb values of "0". 
    // It outputs 0 in hexa when it is suppose to be 00. This is due to 0x not having a number representation.
    if(redHex.length == 1) {
        redHex = "0" + redHex;
    }
    if(greenHex.length == 1) {
        greenHex = "0" + greenHex;
    }
    if(blueHex.length == 1) {
        blueHex = "0" + blueHex;
    }
    return redHex + greenHex + blueHex; //ff0000
}

const hexToRgb = (hex) => {
    // This slices the hex value to three pieces and converts each peace to 16base int
    console.log(hex)
    const r = parseInt(hex.slice(0,2), 16)
    const g = parseInt(hex.slice(2,4), 16)
    const b = parseInt(hex.slice(4,6), 16)
    return `r: ${r}, g: ${g}, b: ${b}`
}

module.exports = {
    rgbToHex, hexToRgb
}