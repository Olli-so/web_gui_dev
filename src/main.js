const express = require ('express');
const converter = require('./converter');
const bodyParser = require("body-parser");
const path = require("path");
const app = express();
const port = 3000

app.use(express.static("public"));
app.use(express.json())

// endpoint http://localhost:3000/rgb-to-hex?red=255&green=128&blue=5
app.post('/rgb-to-hex', (req, res) => {
    console.log(req.body)
    const {r, g, b} = req.body
    // const r = parseInt(req.query.redValue, 10);
    // const g = parseInt(req.query.greenValue, 10);
    // const b = parseInt(req.query.blueValue, 10);
    const hex = converter.rgbToHex(r,g,b);
    res.json({hex});
})
//
//// endpoint http://localhost:3000/hex-to-rgb?hex=ff0000
app.post('/hex-to-rgb', (req, res) => {
    const hexString = req.body.hex
    const rgb = converter.hexToRgb(hexString);
    res.json({rgb});
})

app.listen(port, () => {
    console.log(`http://127.0.0.1:${port}`);
})
