async function hexToRgb() {
    const hex = document.getElementById("hexValue").value;
    const response = await fetch ("/hex-to-rgb", {
        method: "POST", 
        headers: {
            "Content-Type": "application/json"
        }, 
        body: JSON.stringify({hex})
    });
    if (response.ok) {
        const res = await response.json()
        console.log(res.hex)
        document.getElementById("rgb-result").textContent = res.rgb
    } else {
        const error = await response.json()
        document.getElementById("rgb-result").innerText = res.error
    }
}
async function rgbToHex() {
    const r = document.getElementById("redValue").value;
    const g = document.getElementById("greenValue").value;
    const b = document.getElementById("blueValue").value;
    const response = await fetch ("/rgb-to-hex", {
        method: "POST", 
        headers: {
            "Content-Type": "application/json"
        }, 
        body: JSON.stringify({r,g,b})
    });
    if (response.ok) {
        const res = await response.json()
        console.log(res.hex)
        console.log(res)
        document.getElementById("hex-result").textContent = res.hex
    } else {
        const error = await response.json()
        document.getElementById("hex-result").innerText = res.error
    }
}