const { expect } = require("chai");
const conveter = require("../src/converter.js")

describe("Unit testing converter library" , () => {
    var r = undefined;
    var g = undefined;
    var b = undefined;
    var hex = undefined;
    before(() => {
        r = 255;
        g = 255;
        b = 255;
    });
    it("Can convert Rgb values to hex", () => {
        hex = conveter.rgbToHex(r,g,b);
        expect(hex).to.equal("ffffff");
    });
    it("Can convert hex values back to rgb values", () => {
        rgb = conveter.hexToRgb(hex);
        expect(r).to.equal(rgb.r)
        expect(g).to.equal(rgb.g)
        expect(b).to.equal(rgb.b)
    });
    after(() => {
        console.log("Cleanup") // Cant think of anything to put in here..
    })
})