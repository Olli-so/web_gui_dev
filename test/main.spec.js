const { expect } = require("chai");
const app = require("../src/main.js");
const request = require("request");
const PORT = 3000;

describe("Hex and RGB converter API", () => {
    var server = undefined;
    before("Start server before tests", (done) => {
        server = app.listen(PORT, () => {
            console.log(`Server: http://127.0.0.1:${PORT}`);
            done();
        });
    });
    describe("RBG to HEX conversion", () => {
        const endpoint = `http://127.0.0.1:${PORT}/rgb-to-hex?red=255&green=128&blue=0`;
        it("Returns correct status code(200)", (done) => {
            request(endpoint, (error, response, body) => {
                expect(response.statusCode).to.equal(200);
                done();
            });
        });
        it("returns correct hex color code", (done) => {
            request(endpoint, (error, response, body) => {
                expect(body).to.equal("ff8000");
                done(); 
            });
        });
    });
    after("Cleanup and stopping server after tests", (done) => {
        server.close();
        done();
    })
})